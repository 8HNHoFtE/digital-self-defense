| Beschreibung                  | Kosten (Monat) | Kosten (Jahr) |
| ----------------------------- | -------------- | ------------- |
| VServer                       | 7 Euro         | 84 Euro       |
| Cloud Speicher 100GB          | 3 Euro         | 36 Euro       |
| Webhosting / Email            | 12 Euro        | 144 Euro      |
| VPN                           | 9,99 Euro      | 119,88 Euro   |
| Secure Email                  | 4,50 Euro      | 54,00 Euro    |
| ============================= | ============== | ============= |
| *Summe*                       | *36,49 Euro*   | *437,88 Euro* |
