.. Digitale Selbstverteidigung documentation master file, created by
   sphinx-quickstart on Sun Jul 29 16:36:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Digitale Selbstverteidigung's documentation!
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   docs/kosten.md
   docs/medias.md
   docs/dsvgo.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
